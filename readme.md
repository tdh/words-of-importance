This theme - *Words of Importance* - was initially created as an example of how to convert HTML mockups to WordPress themes, as described in the feature article in _Web Designer #213_.

Theme and code by *Thord Daniel Hedengren*. Read more about me and what I do, and the books I write, on [TDH.me](http://tdh.me).

Feature requests and bug reports, as well as translations, should be directed to [@tdh on Twitter](http://twitter.com/tdh). Thanks.