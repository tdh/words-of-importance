<?php get_header(); ?>

	<?php
		// Is there something in the front page sidebar?
		if ( is_active_sidebar( 'front-page' ) ) {
			get_sidebar( 'front' );
		}
	?>

	<section id="main-container">

		<?php
			// Is there something in the sidebar?
			if ( is_active_sidebar( 'side-column' ) ) {
				get_sidebar();
			}
		?>

		<?php 
			// The basic loop
			while ( have_posts() ) : the_post();

			// Load the appropriate content template
			get_template_part( 'content', 'index' );

			// End the loop
			endwhile;

			// Navigation
			get_template_part( 'nav', 'bottom' );

		?>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>