<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<p class="postmeta">
			<?php echo get_avatar( get_the_author_meta( 'user_email' ), $size = '56' ); ?>
			<span class="postmeta-date"><?php the_date(); ?></span> &bull; <span class="postmeta-section"><?php the_category( ' ' ); ?></span>
		</p>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h1>
	</header>

	<section class="entry-content">
	    <?php the_content( 'Read more &rarr;' ); ?>
	</section>

</article>