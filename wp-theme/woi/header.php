<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
<head>
	<meta charset="utf-8">
	<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
	<meta name="author" content="<?php bloginfo( 'name' ); ?>">
	<meta name="viewport" content="initial-scale = 1,user-scalable=no,maximum-scale=1.0">
	<meta name="HandheldFriendly" content="true"/> 

	<title>
	<?php
		// Based on Twenty Eleven
		global $page, $paged;
	
		wp_title( '|', true, 'right' );
	
		// Add the blog name.
		bloginfo( 'name' );
	
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
	
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'ocaore' ), max( $paged, $page ) );
	?>
	</title>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon.png" />

	<?php 
		// Queue threaded comment JavaScript
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	
		// Kick off WordPress
		wp_head();
	?>
</head>
<body <?php body_class(); ?>>

	<div id="outer-wrap">
		<div id="inner-wrap">

			<header id="header-container">

				<a name="top"></a><!-- For link in footer -->

				<nav id="desktop-menu" class="desktop" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'desktop-menu', 'fallback_cb' => false ) ); ?>
				</nav><!-- #desktop-menu ends -->

				<nav id="mobile-menu" class="mobile" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu', 'fallback_cb' => false ) ); ?>
				</nav><!-- #mobile-menu ends -->

				<?php 
					// Is there a custom header?
					if ( get_header_image() != '' ) {
				?>
					<h1 id="site-title">
						<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
							<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo( 'name' ); ?>" />
						</a>
					</h1>

				<?php } 
					// No header image?
					else { 
				?>

					<h1 id="site-title">
						<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
							<?php bloginfo( 'name' ); ?>
						</a>
					</h1>
					<h2 id="site-tagline"><?php bloginfo( 'description' ); ?></h2>

				<?php } // All done ?>
			</header>