<?php get_header(); ?>

	<section id="main-container">

		<article id="post-0" class="page 404">

			<header class="entry-header">
				<h1 class="entry-title-single entry-title-page">
					<?php _e( "There's Nothing Here", "woi" ); ?>
				</h1>
			</header>

			<section class="entry-content">
			    <p><?php _e( "Terribly sorry, but there's nothing here. Have you tried searching for whatever it was you were looking for?", "woi" ); ?></p>
			    <p><?php get_search_form(); ?></p>
			</section>

		</article>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>