<?php get_header(); ?>

	<section id="main-container">

		<?php 
			// The basic loop
			while ( have_posts() ) : the_post();

			// Load the appropriate content template
			get_template_part( 'content', 'single' );

			// End the loop
			endwhile;

			// Include the comments
			comments_template();

		?>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>