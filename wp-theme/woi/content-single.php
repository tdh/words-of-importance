<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<p class="postmeta postmeta-single">
			<span class="postmeta-date"><?php the_date(); ?></span> &bull; <span class="postmeta-section"><?php the_category( ' ' ); ?></span>
		</p>
		<h1 class="entry-title-single">
			<?php the_title(); ?>
		</h1>
		<div class="avatar-single"><?php echo get_avatar( get_the_author_meta( 'user_email' ), $size = '96' ); ?></div>
	</header>

	<section class="entry-content entry-content-single">
	    <?php the_content(); ?>
	    <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woi' ), 'after' => '</div>' ) ); ?>
	</section>

</article>