<?php get_header(); ?>

	<section id="main-container">

		<?php 
			// The basic loop
			while ( have_posts() ) : the_post();

			// Load the appropriate content template
			get_template_part( 'content', 'page' );

			// End the loop
			endwhile;

		?>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>