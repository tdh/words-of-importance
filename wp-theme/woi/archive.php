<?php get_header(); ?>

	<section id="main-container">

		<h1 class="entry-title-single entry-title-page">
		<?php
	    	// Daily archives
	    	if ( is_day() ) :
	    		echo get_the_date();

	    	// Monthly archives
	    	elseif ( is_month() ) :
		    	echo get_the_date( 'F Y' );

	    	// Yearly archives
	    	elseif ( is_year() ) :
	    		echo get_the_date( 'Y' );
		    
	        // Category archives
	        elseif ( is_category() ) :
	        	echo single_cat_title();
	        	
	        // Tag archives
	        elseif ( is_tag() ) :
	        	echo single_tag_title();
	        
	        // Custom taxonomies archives
	        elseif ( is_tax() ) :
	        	echo single_term_title();
	    	
	    	endif; 
	    ?>
	    </h1>

		<?php 
			// The basic loop
			while ( have_posts() ) : the_post();

			// Load the appropriate content template
			get_template_part( 'content', 'index' );

			// End the loop
			endwhile;

			// Navigation
			get_template_part( 'nav', 'bottom' );

		?>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>