<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h1 class="entry-title-single entry-title-page">
			<?php the_title(); ?>
		</h1>
	</header>

	<section class="entry-content entry-content-single">
	    <?php the_content(); ?>
	    <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woi' ), 'after' => '</div>' ) ); ?>
	</section>

</article>