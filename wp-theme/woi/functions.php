<?php

/* THEME SETUP
   Set up basic theme functionality here */
   
function woi_setup() {

	// Add RSS feed links to <head>
	add_theme_support( 'automatic-feed-links' );
	
	// Add custom background support
	add_custom_background();

    // Add custom header support
    $args = array(
        'flex-width' => true,
        'width' => 840,
        'flex-height' => true,
        'height' => 200,
        'header-text' => false
    );
    add_theme_support( 'custom-header', $args );

	// Register menus
    register_nav_menu( 'desktop-menu', __( 'Desktop Menu', 'woi' ) );
    register_nav_menu( 'mobile-menu', __( 'Mobile Menu', 'woi' ) );

}
add_action( 'after_setup_theme', 'woi_setup' );

/* SIDEBARS
   Register widget areas here */

function woi_widgets_init() {

  // Front page widgets
  register_sidebar( array(
      'name' => __( 'Front Page Widget Area', 'woi' ),
      'id' => 'front-page',
      'description' => __( 'Widget area above the post listing on the front page.', 'woi' ),
      'before_widget' => '<li id="%1$s" class="widget-container-front %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
  ) );

  // Side column widget area
  register_sidebar( array(
      'name' => __( 'Side Column Widget Area', 'woi' ),
      'id' => 'side-column',
      'description' => __( 'Widget area in the side column, shown on archives.', 'woi' ),
      'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
      'after_widget' => '</li>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
  ) );

}
add_action( 'widgets_init', 'woi_widgets_init' );


/* COMMENTS
   Callback function for comments */

function woi_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>" class="comment-body">
      <div class="comment-author vcard">
         <?php echo get_avatar($comment,$size='32',$default='<path_to_url>' ); ?>
         <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.') ?></em>
         <br />
      <?php endif; ?>

      <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>

      <?php comment_text() ?>

      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </div>
<?php }


?>