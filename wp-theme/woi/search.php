<?php get_header(); ?>

	<section id="main-container">

		<h1 class="entry-title-single entry-title-page">
			"<?php echo get_search_query(); ?>"
	    </h1>

		<?php 
			// The basic loop
			if ( have_posts() ) : while ( have_posts() ) : the_post();

			// Load the appropriate content template
			get_template_part( 'content', 'index' );

			// End the loop
			endwhile;

			// Navigation
			get_template_part( 'nav', 'bottom' );

			// There were no results
			else :
		?>

		<article id="post-0" class="page search">

			<section class="entry-content">
			    <p><?php _e( "I'm afraid we couldn't find what you were looking for. Why don't you try another search query?", "woi" ); ?></p>
			    <p><?php get_search_form(); ?></p>
			</section>

		</article>

		<?php
			endif;
		?>

	</section><!-- #main-container ends -->

<?php get_footer(); ?>