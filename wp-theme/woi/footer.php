			<footer id="footer-container">
				<p class="copy">
					Copyright &copy; <a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a>
				</p>
				<p class="link-top"><a href="#top">&uarr;</a></p>
			</footer>

		</div><!-- #inner-wrap ends -->
	</div><!-- #outer-wrap ends -->

	<?php wp_footer(); ?>

</body>
</html>